package ru.tsc.panteleev.tm.api.service;

import ru.tsc.panteleev.tm.enumerated.Sort;
import ru.tsc.panteleev.tm.enumerated.Status;
import ru.tsc.panteleev.tm.model.Task;

import java.util.Comparator;
import java.util.Date;
import java.util.List;

public interface ITaskService extends IService<Task> {

    List<Task> findAllByProjectId(String projectId);

    Task create(String name);

    Task create(String name, String description);

    Task create(String name, String description, Date dateBegin, Date dateEnd);

    Task updateByIndex(Integer index, String name, String description);

    Task updateById(String id, String name, String description);

    Task changeStatusByIndex(Integer index, Status status);

    Task changeStatusById(String id, Status status);

}
