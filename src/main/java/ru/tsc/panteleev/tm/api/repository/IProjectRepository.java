package ru.tsc.panteleev.tm.api.repository;

import ru.tsc.panteleev.tm.model.Project;

public interface IProjectRepository extends IRepository<Project>{

    Project create(String name);

    Project create(String name, String description);

}
