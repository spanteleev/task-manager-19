package ru.tsc.panteleev.tm.exception.system;

import ru.tsc.panteleev.tm.exception.AbstractException;

public final class AccessDeniedException extends AbstractException {

    public AccessDeniedException() {
        super("Error! Access denied. Please login and try again...");
    }

}
