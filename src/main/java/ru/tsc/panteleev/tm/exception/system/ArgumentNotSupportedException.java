package ru.tsc.panteleev.tm.exception.system;

import ru.tsc.panteleev.tm.exception.AbstractException;

public final class ArgumentNotSupportedException extends AbstractException {

    public ArgumentNotSupportedException() {
        super("Error! Argument not supported...");
    }

    public ArgumentNotSupportedException(String argument) {
        super("Error! Argument '" + argument + "' not supported...");
    }
}
