package ru.tsc.panteleev.tm.command.system;

import ru.tsc.panteleev.tm.command.AbstractCommand;

import java.util.Collection;

public class ArgumentsCommand extends AbstractSystemCommand {

    public static final String NAME = "arguments";

    public static final String DESCRIPTION = "Show arguments list.";

    public static final String ARGUMENT = "-arg";

    @Override
    public String getName() {
        return NAME;
    }

    @Override
    public String getDescription() {
        return DESCRIPTION;
    }

    @Override
    public String getArgument() {
        return ARGUMENT;
    }

    @Override
    public void execute() {
        final Collection<AbstractCommand> commands = getCommandService().getTerminalCommands();
        for (AbstractCommand command : commands) {
            final String argument = command.getArgument();
            if (argument != null && !argument.isEmpty())
                System.out.println(argument);
        }
    }

}
