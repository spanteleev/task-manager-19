package ru.tsc.panteleev.tm.command.project;

import ru.tsc.panteleev.tm.enumerated.Status;
import ru.tsc.panteleev.tm.util.TerminalUtil;

public class ProjectCompleteByIndexCommand extends AbstractProjectCommand {

    public static final String NAME = "project-complete-by-index";

    public static final String DESCRIPTION = "Complete project by index.";

    @Override
    public String getName() {
        return NAME;
    }

    @Override
    public String getDescription() {
        return DESCRIPTION;
    }

    @Override
    public void execute() {
        System.out.println("[COMPLETE PROJECT BY INDEX]");
        System.out.println("ENTER INDEX:");
        final Integer index = TerminalUtil.nextNumber() - 1;
        getProjectService().changeStatusByIndex(index, Status.COMPLETED);
    }

}
