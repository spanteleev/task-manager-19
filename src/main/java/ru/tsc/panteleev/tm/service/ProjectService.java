package ru.tsc.panteleev.tm.service;

import ru.tsc.panteleev.tm.api.repository.IProjectRepository;
import ru.tsc.panteleev.tm.api.service.IProjectService;
import ru.tsc.panteleev.tm.enumerated.Status;
import ru.tsc.panteleev.tm.exception.entity.ProjectNotFoundException;
import ru.tsc.panteleev.tm.exception.field.*;
import ru.tsc.panteleev.tm.model.Project;
import java.util.Date;

public class ProjectService extends AbstractService<Project, IProjectRepository> implements IProjectService {

    public ProjectService(IProjectRepository repository) {
        super(repository);
    }

    @Override
    public Project create(final String name) {
        if (name == null || name.isEmpty()) throw new NameEmptyException();
        return repository.create(name);
    }

    @Override
    public Project create(final String name, final String description) {
        if (name == null || name.isEmpty()) throw new NameEmptyException();
        if (description == null || description.isEmpty()) throw new DescriptionEmptyException();
        return repository.create(name, description);
    }

    @Override
    public Project create(String name, String description, Date dateBegin, Date dateEnd) {
        final Project project = create(name, description);
        if (project == null) throw new ProjectNotFoundException();
        project.setDateBegin(dateBegin);
        project.setDateEnd(dateEnd);
        return project;
    }

    @Override
    public Project updateByIndex(Integer index, String name, String description) {
        if (index == null || index<0) throw new IndexIncorrectException();
        final Project project = findByIndex(index);
        if (project == null) throw new ProjectNotFoundException();
        if (name == null || name.isEmpty()) throw new NameEmptyException();
        if (description == null || description.isEmpty()) throw new DescriptionEmptyException();
        project.setName(name);
        project.setDescription(description);
        return project;
    }

    @Override
    public Project updateById(String id, String name, String description) {
        if (id == null || id.isEmpty()) throw new IdEmptyException();
        final Project project = findById(id);
        if (project == null) throw new ProjectNotFoundException();
        if (name == null || name.isEmpty()) throw new NameEmptyException();
        if (description == null || description.isEmpty()) throw new DescriptionEmptyException();
        project.setName(name);
        project.setDescription(description);
        return project;
    }

    @Override
    public Project changeStatusByIndex(Integer index, Status status) {
        if (index == null || index<0) throw new IndexIncorrectException();
        final Project project = findByIndex(index);
        if (project == null) throw new ProjectNotFoundException();
        if (status == null)  throw new StatusIncorrectException();
        project.setStatus(status);
        return project;
    }

    @Override
    public Project changeStatusById(String id, Status status) {
        if (id == null || id.isEmpty()) throw new IdEmptyException();
        final Project project = findById(id);
        if (project == null) throw new ProjectNotFoundException();
        if (status == null)  throw new StatusIncorrectException();
        project.setStatus(status);
        return project;
    }

}
