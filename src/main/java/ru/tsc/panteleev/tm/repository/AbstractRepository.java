package ru.tsc.panteleev.tm.repository;

import ru.tsc.panteleev.tm.api.repository.IRepository;
import ru.tsc.panteleev.tm.enumerated.Sort;
import ru.tsc.panteleev.tm.exception.entity.ModelNotFoundException;
import ru.tsc.panteleev.tm.exception.field.IndexIncorrectException;
import ru.tsc.panteleev.tm.model.AbstractModel;
import java.util.ArrayList;
import java.util.Comparator;
import java.util.List;

public class AbstractRepository<M extends AbstractModel> implements IRepository<M> {

    protected final List<M> models = new ArrayList<>();

    @Override
    public M add(M model) {
        models.add(model);
        return model;
    }

    @Override
    public List findAll() {
        return models;
    }

    @Override
    public List findAll(Comparator comparator) {
        final List<M> result = new ArrayList<>(models);
        result.sort(comparator);
        return result;
    }

    @Override
    public List<M> findAll(Sort sort) {
        final List<M> result = new ArrayList<>(models);
        result.sort(sort.getComparator());
        return result;
    }

    @Override
    public M findById(String id) {
        for (final M model: models) {
            if (id.equals(model.getId())) return model;
        }
        throw new ModelNotFoundException();
    }

    @Override
    public M findByIndex(Integer index) {
        if (index>getSize()) throw new IndexIncorrectException();
        return models.get(index);
    }

    @Override
    public M remove(M model) {
        models.remove(model);
        return model;
    }

    @Override
    public M removeById(String id) {
        final M model = findById(id);
        if (model == null) return null;
        return remove(model);
    }

    @Override
    public M removeByIndex(Integer index) {
        final M model = findByIndex(index);
        if (model == null) return null;
        return remove(model);
    }

    @Override
    public void clear() {
        models.clear();
    }

    @Override
    public boolean existsById(String id) {
        return findById(id) != null;
    }

    @Override
    public int getSize() {
        return models.size();
    }
}
