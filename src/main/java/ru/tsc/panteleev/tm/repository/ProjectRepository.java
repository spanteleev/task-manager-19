package ru.tsc.panteleev.tm.repository;

import ru.tsc.panteleev.tm.api.repository.IProjectRepository;
import ru.tsc.panteleev.tm.model.Project;

public class ProjectRepository extends AbstractRepository<Project> implements IProjectRepository {

    @Override
    public Project create(final String name) {
        final Project project = new Project();
        project.setName(name);
        return add(project);
    }

    @Override
    public Project create(final String name, final String description) {
        final Project project = new Project();
        project.setName(name);
        project.setDescription(description);
        return add(project);
    }

}
