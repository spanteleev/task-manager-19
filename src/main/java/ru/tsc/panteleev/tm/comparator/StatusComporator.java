package ru.tsc.panteleev.tm.comparator;

import ru.tsc.panteleev.tm.api.model.IHasStatus;
import java.util.Comparator;

public enum StatusComporator implements Comparator<IHasStatus> {

    INSTANCE;

    @Override
    public int compare(IHasStatus o1, IHasStatus o2) {
        if (o1 == null || o2 == null) return 0;
        if (o1.getStatus() == null || o2.getStatus() == null) return 0;
        return o1.getStatus().compareTo(o2.getStatus());
    }

}
